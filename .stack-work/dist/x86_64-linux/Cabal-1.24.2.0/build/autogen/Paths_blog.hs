{-# LANGUAGE CPP #-}
{-# OPTIONS_GHC -fno-warn-missing-import-lists #-}
{-# OPTIONS_GHC -fno-warn-implicit-prelude #-}
module Paths_blog (
    version,
    getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir,
    getDataFileName, getSysconfDir
  ) where

import qualified Control.Exception as Exception
import Data.Version (Version(..))
import System.Environment (getEnv)
import Prelude

#if defined(VERSION_base)

#if MIN_VERSION_base(4,0,0)
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#else
catchIO :: IO a -> (Exception.Exception -> IO a) -> IO a
#endif

#else
catchIO :: IO a -> (Exception.IOException -> IO a) -> IO a
#endif
catchIO = Exception.catch

version :: Version
version = Version [0,1,0,0] []
bindir, libdir, dynlibdir, datadir, libexecdir, sysconfdir :: FilePath

bindir     = "/home/tom/24_Blog/blog/.stack-work/install/x86_64-linux/31b3225b541ad94bc66cd7c54898a94dac34015cc14949ee1c85d1e9f14a3213/8.0.2/bin"
libdir     = "/home/tom/24_Blog/blog/.stack-work/install/x86_64-linux/31b3225b541ad94bc66cd7c54898a94dac34015cc14949ee1c85d1e9f14a3213/8.0.2/lib/x86_64-linux-ghc-8.0.2/blog-0.1.0.0"
dynlibdir  = "/home/tom/24_Blog/blog/.stack-work/install/x86_64-linux/31b3225b541ad94bc66cd7c54898a94dac34015cc14949ee1c85d1e9f14a3213/8.0.2/lib/x86_64-linux-ghc-8.0.2"
datadir    = "/home/tom/24_Blog/blog/.stack-work/install/x86_64-linux/31b3225b541ad94bc66cd7c54898a94dac34015cc14949ee1c85d1e9f14a3213/8.0.2/share/x86_64-linux-ghc-8.0.2/blog-0.1.0.0"
libexecdir = "/home/tom/24_Blog/blog/.stack-work/install/x86_64-linux/31b3225b541ad94bc66cd7c54898a94dac34015cc14949ee1c85d1e9f14a3213/8.0.2/libexec"
sysconfdir = "/home/tom/24_Blog/blog/.stack-work/install/x86_64-linux/31b3225b541ad94bc66cd7c54898a94dac34015cc14949ee1c85d1e9f14a3213/8.0.2/etc"

getBinDir, getLibDir, getDynLibDir, getDataDir, getLibexecDir, getSysconfDir :: IO FilePath
getBinDir = catchIO (getEnv "blog_bindir") (\_ -> return bindir)
getLibDir = catchIO (getEnv "blog_libdir") (\_ -> return libdir)
getDynLibDir = catchIO (getEnv "blog_dynlibdir") (\_ -> return dynlibdir)
getDataDir = catchIO (getEnv "blog_datadir") (\_ -> return datadir)
getLibexecDir = catchIO (getEnv "blog_libexecdir") (\_ -> return libexecdir)
getSysconfDir = catchIO (getEnv "blog_sysconfdir") (\_ -> return sysconfdir)

getDataFileName :: FilePath -> IO FilePath
getDataFileName name = do
  dir <- getDataDir
  return (dir ++ "/" ++ name)
